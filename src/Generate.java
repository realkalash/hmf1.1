public class Generate {
    public static Shop[] generate() {
        Shop[] shops = new Shop[4];
        {
            Smartphone[] smartphones = new Smartphone[5];
            smartphones[0] = new Smartphone("Apple", 5.5f, 30250);
            smartphones[1] = new Smartphone("Samsung", 6f, 7999);
            smartphones[2] = new Smartphone("Xiaomi", 5.5f, 5499);
            smartphones[3] = new Smartphone("Apple", 4.7f, 16653);
            smartphones[4] = new Smartphone("Huawei", 6f, 32999);

            shops[0] = new Shop("Цитрус", "Олимпийская 8", smartphones);
        }
        {
            Smartphone[] smartphones = new Smartphone[3];
            smartphones[0] = new Smartphone("Xiaomi", 5.5f, 5000);
            smartphones[1] = new Smartphone("Samsung", 5.7f, 7600);
            smartphones[2] = new Smartphone("Huawei", 5.7f, 31000);

            shops[1] = new Shop("Алло", "Сумская 25", smartphones);
        }
        {
            Smartphone[] smartphones = new Smartphone[2];
            smartphones[0] = new Smartphone("Apple", 4.7f, 12999);
            smartphones[1] = new Smartphone("Apple", 5.5f, 25000);

            shops[2] = new Shop("Apple", "Героев Сталинграда", smartphones);
        }
        {
            Smartphone[] smartphones = new Smartphone[4];
            smartphones[0] = new Smartphone("Apple", 4.7f, 5500);
            smartphones[1] = new Smartphone("Samsung", 6f, 6999);
            smartphones[2] = new Smartphone("Xiaomi", 5.5f, 4599);
            smartphones[3] = new Smartphone("Huawei", 6f, 14000);

            shops[3] = new Shop("CELL", "Ньютона 110", smartphones);
        }
        return shops;
    }
}
