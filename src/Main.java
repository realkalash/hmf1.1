import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Shop[] shops = Generate.generate();

        Scanner in = new Scanner(System.in);
        System.out.println("Выберите одну из следующих функций:\n" +
                "Найти самый дешёвый смартфон\n" +
                "Найти смартфоны по названию\n" +
                "Найти дешёвые смартфоны с самым большим экраном");
        String inputStr = in.nextLine();

        if (inputStr.equalsIgnoreCase("Найти самый дешёвый смартфон") || inputStr.equalsIgnoreCase("1")) {
            for (Shop shop : shops) {
                Smartphone smartphone = shop.searchForTheCheapestAmartphone();
                if (smartphone != null) {
                    Shop searchByShops = shops[0];
                    if (searchByShops.isCheapestInTheShops(shop)) {
                        searchByShops = shop;
                        System.out.println(String.format("Самый дешёвый смартфон в Магазине: %s. Это: %s", searchByShops.getName(),
                                smartphone.getInfo()
                        ));
                    }
                }
            }
        }
        if (inputStr.equalsIgnoreCase("Найти смартфоны по названию") || inputStr.equalsIgnoreCase("2")) {

            inputStr = in.nextLine();

            for (Shop shop : shops) {
                Smartphone smartphone = shop.manufacturerSmartphone(inputStr);
                if (smartphone != null) {
                    System.out.println(String.format("В магазине:%s найден смартфон: %s ",
                            shop.getName(), smartphone.getInfo()));
                }
            }
        }
        if (inputStr.equalsIgnoreCase("Найти дешёвые смартфоны с самым большим экраном") || inputStr.equalsIgnoreCase("3")) {

            for (Shop shop : shops) {
                Smartphone smartphone = shop.smartphoneWithTheLargestDiagonal();
                if (smartphone != null) {
                    Shop searchByShops = shops[0];
                    if (searchByShops.isCheapestInTheShops(shop)) {
                        searchByShops = shop;
                        System.out.println(String.format("Самыйе дешёвые смартфоны с самым большим экраном: %s в магазне %s",
                                smartphone.getInfo(), searchByShops.getName()));
                    }
                }
            }
        }
    }
}